<?php
	require ('Animal.php');
	require_once('Frog.php');
	require_once('Ape.php');
	
	echo "Release 0 <br>";
	$sheep = new Animal("shaun");
	echo "Nama : ".$sheep->name."<br>"; // "shaun"
	echo "Jumlah Kaki : ".$sheep->get_legs()."<br>"; // 2
	echo "Berdarah Dingin : ".$sheep->get_cold_blooded()."<br>"; // false
	echo "<br>";
	echo "Release 1 <br>";
	$sungokong = new Ape("kera sakti");
	echo "Nama : ".$sungokong->name."<br>"; 
	echo "Jumlah Kaki : ".$sungokong->get_legs()."<br>";
	echo "Berdarah Dingin : ".$sheep->get_cold_blooded()."<br>"; 
	echo $sungokong->yell(); // "Auooo"
	echo "<br><br>";
	$kodok = new Frog("buduk");
	echo "Nama : ".$kodok->name."<br>"; // 
	echo "Jumlah Kaki : ".$kodok->get_legs()."<br>"; // 
	echo "Berdarah Dingin : ".$kodok->get_cold_blooded()."<br>";
	echo $kodok->jump() ; // "hop hop"

	
?>